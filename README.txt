RemoteCode Filter
=================

Description
-----------
RemoteCode is a Drupal module providing a content filter to allow embedding the
content of a file hosted on a remote site, like GitHub, BitBucket or other
similar sites.

The idea is to ease the inclusion of the source code of such files without
"copy and paste" operations, which are prone to mistakes or are inconvenient.
The goal is that the embedded source code is always precise because it is
referencing a specific revision, or is always up to date because it is
referencing the HEAD revision.

After receiving the content of the referenced remote file, it wraps them for
syntax hightlighting, which is achieved through the "GeSHi Filter" module.

Dependencies
------------
The "GeSHi Filter" module is required, and it is used for syntax hightlighting.

Installation
------------
1. Install the GeSHi Filter module, following all of the instructions to get it
working. It is available at https://www.drupal.org/project/geshifilter

2. Extract the RemoteCode Filter module contents to a modules directory of
Drupal, so the entire destination is like sites/all/modules/remotecode under
the root directory of your Drupal site.

3. Enable this module and the GeSHi Filter module

Configuration
-------------
1. In the "Text Format" configuration section, proceed to configure the desired
format that will allow the use of the RemoteCode filter. Note that this can be
done for one or more formats.

2. Enable both the GeSHi filter and the RemoteCode filter

3. Adjust the filter processing order so the RemoteCode filter runs before the
GeSHi filter

4. Take care about other filters interfering with the right working of the
GeSHi filter, because they can limit the allowed HTML tags or completely block
them. For more details read the GeSHi filter documentation

Usage
-----
When editing the content body of your articles, pages or blog posts, just use
the same GeSHi filter tags, like this:

<code
  lang=python
  url=https://bitbucket.org/tortoisehg/thg/raw/54f7d82b789f3f70109eb8633b7f55750569c16f/tortoisehg/hgqt/__init__.py>
</code>

The required attribute is the URL from which the remote file content will be
downloaded. Any other attribute in the form key=value will be passed to the
GeSHi filter in the form key="value". Note that the name expected for the URL
attribute is "url" (lowercase only)

When the content is finally rendered, the user will see the real content of
that file, with its syntax highlighted. Note that for this to happen, all of
the filters settings must be right.

Miscellaneous notes
-------------------

- It's possible to use authentication parameters, following the specifications
for the drupal_http_request function of Drupal core; however, is your
responsibility to do it in a secure way, because a misconfiguration of the
text format filters can leave your credentiales publicly exposed.

- When a given page is saved the subsequent access uses the Drupal cache system,
so an update in the remote file will require saving the page again, in order to
show it updated within the page body.

- Connections error detected when trying to download the remote content, will
be logged using Drupal's watchdog function and the error report of your site
will show the details.

Author
------
Rodolfo Castellanos
